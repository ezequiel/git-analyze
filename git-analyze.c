/*
 * Collabora's git repo analysis tool.
 *
 * With bits and pieces copied from the
 * great examples in the libgit2 repo:
 *
 *   Written by the libgit2 contributors
 *
 *   To the extent possible under law, the author(s) have dedicated all copyright
 *   and related and neighboring rights to this software to the public domain
 *   worldwide. This software is distributed without any warranty.
 *
 *   You should have received a copy of the CC0 Public Domain Dedication along
 *   with this software. If not, see
 *   <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <git2.h>

#define progress_string "###############################################################################"
#define progress_width 79

#define for_each_commit(repo, walker, commit) \
	for (git_oid oid; \
	     !git_revwalk_next(&oid, walker) && !git_commit_lookup(&commit, repo, &oid); \
	     git_commit_free(commit))

#define COMMIT_FILTER_BY_MESSAGE	(0x1 << 1)
#define COMMIT_FILTER_BY_MAX_PARENTS	(0x1 << 2)

#define AUTHOR_LENGTH 256
#define EMAIL_LENGTH 256
#define FILENAME_LENGTH 256

struct commit_filter {
	int type;
	const char *message;
	unsigned int max_parents;
	// Add fields to filter by other commit properties.
};

struct commit {
	struct commit *parent;
	char author[AUTHOR_LENGTH];
	char email[EMAIL_LENGTH];
	char hash[GIT_OID_HEXSZ + 1];
};

struct file {
//	list of commits;
//	difference (a sort of "statistical" difference between upstream and current, a double between 0 and 1.0)
	size_t insertions;
	size_t deletions;
	size_t commit_count;
	char filename[FILENAME_LENGTH];
};

struct directory {
//	list of directories;
//	list of files;
	size_t insertions;
	size_t deletions;
	size_t commit_count;
	char dirname[FILENAME_LENGTH];
};

// The tip is the topologically newest commit
static struct commit *commit_tip;
static struct directory *root;

static int usage(const char *prog)
{
	fprintf(stderr, "usage: %s <path to repo>\n", prog);
	exit(EXIT_FAILURE);
}

static int panic(const char *msg)
{
	fprintf(stderr, msg);
	exit(EXIT_FAILURE);
}

static void print_progress(unsigned int progress, unsigned int total)
{
	double p = (double)progress/total;
	unsigned int val = (p * 100);
	unsigned l_pad = (int) (p * progress_width);
	unsigned r_pad = progress_width - l_pad;

	printf("\r%3d - %3d%% [%.*s%*s]", progress, val, l_pad, progress_string, r_pad, "");
	fflush(stdout);
}

static void split_first_dir(char **dir, char **base, const char *path)
{
	char *next;

	next = strpbrk(path + 1, "\\/");

	// Get first directory, if any
	if (next && next - path)
		*dir = strndup(path, next - path);
	else
		*dir = NULL;

	// Get remaining string
	if (next)
		*base = strdup(next + 1);
	else
		*base = strdup(path);
}

static int filter_commit(const git_commit *commit, struct commit_filter *filter)
{
	const char *message;
	unsigned int parents;
	int filtered = 0;

	if (filter->type & COMMIT_FILTER_BY_MESSAGE) {
		message = git_commit_message(commit);
		if (!message)
			panic("cannot get commit message\n");
		if (!filter->message)
			panic("empty filter commit message\n");
		filtered |= !strstr(message, filter->message);
	}

	if (filter->type & COMMIT_FILTER_BY_MAX_PARENTS) {
		parents = git_commit_parentcount(commit);
		filtered |= (parents > filter->max_parents);
	}
	return filtered;
}

static struct commit *push_commit(git_commit *commit)
{
	const git_signature *signature;
	struct commit *new_commit;

	new_commit = calloc(1, sizeof(struct commit));

	if (commit_tip)
		new_commit->parent = commit_tip;
	commit_tip = new_commit;

	signature = git_commit_author(commit);
	strncpy(new_commit->author, signature->name, sizeof(new_commit->author) - 1);
	strncpy(new_commit->email, signature->email, sizeof(new_commit->email) - 1);
	git_oid_tostr(new_commit->hash, sizeof(new_commit->hash), git_commit_id(commit));

	return commit_tip;
}

struct file *get_file(struct directory *dir, const char *filename)
{
	return NULL;
}

struct directory *get_dir(struct directory *dir, const char *dirname)
{
	return NULL;
}

void push_file_stats(struct directory *dir, const char *filename, git_diff_stats *diff_stats)
{
	struct file *f;

	f = get_file(dir, filename);
	if (!f) {
		f = calloc(1, sizeof(struct file));
//		dir->files.add(f);
		strncpy(f->filename, filename, sizeof(f->filename) - 1);
	}
	f->insertions += git_diff_stats_insertions(diff_stats);
	f->deletions += git_diff_stats_deletions(diff_stats);
	f->commit_count++;
}

void push_path_stats(struct directory *dir, const char *path, git_diff_stats *diff_stats)
{
	char *dir_name, *base_path;

	dir->insertions = git_diff_stats_insertions(diff_stats);
	dir->deletions = git_diff_stats_deletions(diff_stats);
	dir->commit_count++;

	split_first_dir(&dir_name, &base_path, path);

	if (!dir_name) {
		push_file_stats(dir, base_path, diff_stats);
	} else {
		struct directory *d;

		d = get_dir(dir, dir_name);
		if (!d) {
			d = calloc(1, sizeof(struct directory));
//			dir->directories.add(d);
			strncpy(d->dirname, dir_name, sizeof(d->dirname) - 1);
		}
		push_path_stats(dir, base_path, diff_stats);
	}
}

static void push_commit_to_tree(git_repository *repo, git_commit *commit, struct commit *c)
{
	git_commit *parent;
	git_tree *a, *b;
	git_diff *diff;
        git_diff_options o;
	size_t num_deltas, i;
	int ret;

	git_diff_options_init(&o, 1);
	git_commit_parent(&parent, commit, 0);
	git_commit_tree(&a, parent);
	git_commit_tree(&b, commit);

	//// Attention: This call is _super_ expensive!
	git_diff_tree_to_tree(&diff, repo, a, b, &o);
	////

	num_deltas = git_diff_num_deltas(diff);
        for (i = 0; i < num_deltas; i++) {
		const git_diff_delta *delta;
		git_diff_stats *diff_stats;
		const char *path;

		delta =	git_diff_get_delta(diff, i);
		ret = git_diff_get_delta_stats(&diff_stats, diff, i);
		if (ret)
			continue;
		// Get the path
		if (delta->new_file.path)
			path = delta->new_file.path;
		else
			path = delta->old_file.path;

		push_path_stats(root, path, diff_stats);
		git_diff_stats_free(diff_stats);
	}

	git_diff_free(diff);
	git_tree_free(a);
	git_tree_free(b);
	git_commit_free(parent);
}

#if 0
static void print_commit_oneline(git_commit *commit)
{
	char buf[GIT_OID_HEXSZ + 1];

	git_oid_tostr(buf, sizeof(buf), git_commit_id(commit));
	printf("%s %s\n", buf, git_commit_summary(commit));
}

static void print_commit_tree(git_repository *repo, git_commit *commit)
{
	git_commit *parent;
	git_tree *a, *b;
	git_diff *diff;
        git_diff_options o;

	git_diff_options_init(&o, 1);
	git_commit_parent(&parent, commit, 0);
	git_commit_tree(&a, parent);
	git_commit_tree(&b, commit);

	// This call is super expensive
	git_diff_tree_to_tree(&diff, repo, a, b, &o);

	git_diff_foreach(diff, file_cb, NULL, NULL, NULL, NULL);

	git_diff_free(diff);
	git_tree_free(a);
	git_tree_free(b);
	git_commit_free(parent);
}
#endif

int main(int argc, char* argv[])
{
	git_repository *repo;
	git_revwalk *walker;
	git_commit *commit = NULL;
	struct commit_filter filter = {};
	size_t total_commits, commit_count;
	int ret;

	if (argc < 2)
		usage(argv[0]);

	// We filter merge commits by default
	filter.type = COMMIT_FILTER_BY_MAX_PARENTS;
	filter.max_parents = 1;

	// TODO: We will use this with custom options,
	// such as --chromium --upstream --fromlist
	if (argc > 2) {
		filter.type |= COMMIT_FILTER_BY_MESSAGE;
		filter.message = argv[2];
	}

	git_libgit2_init();

	ret = git_repository_open_ext(&repo, argv[1], 0, NULL);
	if (ret)
		panic("cannot open repo\n");
	
	ret = git_revwalk_new(&walker, repo);
	if (ret)
		panic("cannot allocate revision walker\n");

	// From the spec:
	// ""
	// The initial call to this method is not blocking
	// when iterating through a repo with a time-sorting mode.
	// ""
	// Which seems to imply that time-sorting is the least
	// expensive.
	git_revwalk_sorting(walker, GIT_SORT_TIME);
	ret = git_revwalk_push_range(walker, "v4.19..origin/chromeos-4.19");
	if (ret)
		panic("cannot allocate revision walker\n");

	total_commits = 0;
	for_each_commit(repo, walker, commit) {
		if (filter_commit(commit, &filter))
			continue;
		total_commits++;
	}

	git_revwalk_reset(walker);
	git_revwalk_sorting(walker, GIT_SORT_REVERSE | GIT_SORT_TOPOLOGICAL);
	ret = git_revwalk_push_range(walker, "v4.19.75..origin/chromeos-4.19");
	if (ret)
		panic("cannot allocate revision walker\n");

	// XXX: Processing commits is a heavy operation.
	// Specifically, generating the diff for a commit
	// versus its parent is a heavy CPU-bound task.
	// This could be parallelized, but it would have
	// to be made thread-safe first.
	printf("Processing %ld commits ...\n", total_commits);
	commit_count = 0;

	root = calloc(1, sizeof(struct directory));
	strncpy(root->dirname, "/", sizeof(root->dirname) - 1);

	for_each_commit(repo, walker, commit) {
		struct commit *c;

		if (filter_commit(commit, &filter))
			continue;
		c = push_commit(commit);

		push_commit_to_tree(repo, commit, c);
		print_progress(++commit_count, total_commits);
	}

	git_revwalk_free(walker);
	git_repository_free(repo);
	git_libgit2_shutdown();

	exit(EXIT_SUCCESS);
}
