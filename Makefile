CROSS_COMPILE ?=

CC	?= $(CROSS_COMPILE)gcc
LDFLAGS	?=
CFLAGS	?= -O3 -W -Wall -std=gnu99 `pkg-config --cflags libgit2` -Wno-unused-parameter -Wno-unused-function
LIBS	:= `pkg-config --libs libgit2`

%.o : %.c
	$(CC) $(CFLAGS) -c -o $@ $<

git-analyze: git-analyze.o
	$(CC) $(LDFLAGS) -o $@ $^ $(LIBS)

clean:
	-rm -f *.o git-analyze
